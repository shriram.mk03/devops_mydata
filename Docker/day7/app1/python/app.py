from flask import Flask, jsonify
import mysql.connector

app = Flask(__name__)

@app.route('/')
def root():
    return "<h1>welcome to my application</h1>"

@app.route('/product')
def get_products():
    connection = mysql.connector.connect(host="db", user="root", password="root", database="mydb")
    cursor = connection.cursor()
    statement = "select id, title, price from product"
    cursor.execute(statement)

    products = []
    data = cursor.fetchall()
    for (id, title, price) in data:
        products.append({
            "id": id,
            "title": title,
            "price": price
        })

    cursor.close()
    connection.close()
    return jsonify(products)


app.run(host="0.0.0.0", port="4000")