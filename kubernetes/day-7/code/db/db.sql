create table product (id integer primary key auto_increment, title varchar(100), price float);

insert into product (title, price) values ('product 1', 100);
insert into product (title, price) values ('product 2', 200);
insert into product (title, price) values ('product 3', 300);
insert into product (title, price) values ('product 4', 400);
insert into product (title, price) values ('product 5', 500);