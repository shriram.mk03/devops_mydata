const express = require('express')
const mysql = require('mysql')

const app = express()

app.get('/', (request, response) => {
    response.send('welcome to node application')
})

app.get('/product', (request, response) => {
    const connection = mysql.createConnection({
        host: 'db',
        user: 'root',
        password: 'root',
        database: 'mydb'
    })

    connection.query('select id, title, price from product', (error, products) => {
        connection.end()
        console.log(`error: ${error}`)
        console.log(`products: ${products}`)
        response.send(products)
    })
})

app.listen(8000, '0.0.0.0', () => {
    console.log('server started  on port 8000')
})